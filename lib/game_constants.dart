import 'package:flutter/widgets.dart';

const boardSize = 320.0;
const pieceSize = 20.0;
const textPadding = 32.0;

const themePalette = {
  'color1': Color.fromARGB(255, 0, 18, 25),
  'color2': Color.fromARGB(255, 0, 95, 115),
  'color3': Color.fromARGB(255, 10, 147, 150),
  'color4': Color.fromARGB(255, 148, 210, 189),
  'color5': Color.fromARGB(255, 233, 216, 166),
  'color6': Color.fromARGB(255, 238, 155, 0),
  'color7': Color.fromARGB(255, 202, 103, 2),
  'color8': Color.fromARGB(255, 187, 62, 3),
  'color9': Color.fromARGB(255, 174, 32, 18),
  'color10': Color.fromARGB(255, 155, 34, 38),
};
