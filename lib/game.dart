import 'package:flutter/widgets.dart';
import 'board.dart';
import 'game_constants.dart';

class Game extends StatelessWidget {
  const Game({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          color:
              themePalette['color5'] ?? const Color.fromARGB(255, 255, 0, 255),
          width: boardSize,
          height: textPadding,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Align(
                  alignment: Alignment.centerLeft,
                  child: Text('Score:',
                      style: DefaultTextStyle.of(context)
                          .style
                          .apply(fontSizeFactor: 2.0))),
              Align(
                  alignment: Alignment.centerRight,
                  child: Text('Speed:',
                      style: DefaultTextStyle.of(context)
                          .style
                          .apply(fontSizeFactor: 2.0)))
            ],
          ),
        ),
        const Board()
      ],
    );
  }
}
