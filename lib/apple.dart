import 'package:flutter/widgets.dart';
import 'package:snake/game_constants.dart';

class Apple extends StatelessWidget {
  const Apple({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var color = themePalette['color8'] ?? const Color(0xFFFF0000);
    return Container(
      width: pieceSize,
      height: pieceSize,
      decoration: BoxDecoration(
          color: color,
          border: Border.all(color: color),
          borderRadius: BorderRadius.circular(pieceSize)),
    );
  }
}
