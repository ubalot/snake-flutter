import 'package:flutter/material.dart';
import 'game.dart';
import 'game_constants.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Snake',
        home: const Home(),
        theme: ThemeData(fontFamily: 'Pixel Emulator'));
  }
}

class Home extends StatelessWidget {
  const Home({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
        appBar: AppBar(
          backgroundColor: themePalette['color3'] ??
              const Color.fromARGB(255, 255, 255, 255),
          foregroundColor:
              themePalette['color5'] ?? const Color.fromARGB(255, 0, 0, 0),
          title: const Text('Snake'),
          actions: [
            PopupMenuButton(
              icon: const Icon(Icons.more_vert),
              itemBuilder: (BuildContext context) => <PopupMenuEntry>[
                const PopupMenuItem(
                  child: ListTile(
                    leading: Icon(Icons.speed),
                    title: Text('Speed'),
                  ),
                ),
                const PopupMenuItem(
                  child: ListTile(
                    leading: Icon(Icons.category),
                    title: Text('Mode'),
                  ),
                ),
              ],
            ),
          ],
        ),
        backgroundColor: const Color(0xFF000000),
        body: const Center(
          // Center is a layout widget. It takes a single child and positions it
          // in the middle of the parent.
          child: Game(),
        ));
  }
}
