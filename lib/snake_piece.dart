import 'package:flutter/widgets.dart';
import 'package:snake/game_constants.dart';

class SnakePiece extends StatelessWidget {
  const SnakePiece({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var color = themePalette['color1'] ?? const Color(0xFF0080FF);
    var backgroundColor = themePalette['color3'] ?? color;
    return Container(
      width: pieceSize,
      height: pieceSize,
      decoration: BoxDecoration(
          color: color, border: Border.all(color: backgroundColor)),
    );
  }
}
