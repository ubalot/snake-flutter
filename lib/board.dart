import 'dart:async';
import 'dart:math';

import 'package:flutter/widgets.dart';
import 'package:snake/apple.dart';
import 'package:snake/failure.dart';
import 'package:snake/game_constants.dart';
import 'package:snake/snake_piece.dart';
import 'package:snake/point.dart';
import 'package:snake/splash.dart';
import 'package:snake/victory.dart';

class Board extends StatefulWidget {
  const Board({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _BoardState();
}

enum Direction { left, right, up, down }

enum GameState { splash, running, victory, failure }

class _BoardState extends State<Board> {
  var _eventsQueue = [];
  var _snakePiecePositions = [];
  late Point _applePosition;
  late Timer _timer;
  late Timer _eventsTimer;
  Direction _direction = Direction.up;
  bool _directionChanged = false;
  var _gameState = GameState.splash;

  @override
  Widget build(BuildContext context) {
    var backgroundColor =
        themePalette['color3'] ?? const Color.fromARGB(255, 223, 204, 204);

    return Container(
        color: backgroundColor,
        width: boardSize,
        height: boardSize,
        child: Focus(
            onKey: (node, event) {
              return _handleKey(event);
            },
            child: GestureDetector(
              behavior: HitTestBehavior.opaque,
              onTapUp: (tapUpDetails) {
                _handleTap(tapUpDetails);
              },
              child: _getBoardChildBasedOnGameState(),
            )));
  }

  Widget _getBoardChildBasedOnGameState() {
    Widget child;

    switch (_gameState) {
      case GameState.splash:
        child = const Splash();
        break;

      case GameState.running:
        List<Positioned> snakePiecesAndApple = [];
        for (var i in _snakePiecePositions) {
          snakePiecesAndApple.add(Positioned(
            left: i.x * pieceSize,
            top: i.y * pieceSize,
            child: const SnakePiece(),
          ));
        }

        final apple = Positioned(
          left: _applePosition.x * pieceSize,
          top: _applePosition.y * pieceSize,
          child: const Apple(),
        );

        snakePiecesAndApple.add(apple);

        child = Stack(children: snakePiecesAndApple);
        break;

      case GameState.victory:
        _timer.cancel();
        _eventsTimer.cancel();
        child = const Victory();
        break;

      case GameState.failure:
        _timer.cancel();
        _eventsTimer.cancel();
        child = const Failure();
        break;
    }

    return child;
  }

  void _onTimerTick(Timer timer) {
    _move();

    if (_isWallCollision()) {
      _changeGameState(GameState.failure);
      return;
    }

    if (_isSnakeCollision()) {
      _changeGameState(GameState.failure);
      return;
    }

    if (_isAppleCollision()) {
      if (_isBoardFilled()) {
        _changeGameState(GameState.victory);
      } else {
        _generateNewApple();
        _grow();
      }
      return;
    }
  }

  void _onEventsTimerTick(Timer timer) {
    _evaluateKey();
  }

  void _grow() {
    setState(() {
      _snakePiecePositions.insert(0, _getNewHeadPosition());
    });
  }

  void _move() {
    setState(() {
      _snakePiecePositions.insert(0, _getNewHeadPosition());
      _snakePiecePositions.removeLast();
      _directionChanged = false;
    });
  }

  bool _isWallCollision() {
    var currentHeadPos = _snakePiecePositions.first;

    if (currentHeadPos.x < 0 ||
        currentHeadPos.y < 0 ||
        currentHeadPos.x >= boardSize / pieceSize ||
        currentHeadPos.y >= boardSize / pieceSize) {
      return true;
    }

    return false;
  }

  bool _isAppleCollision() {
    if (_snakePiecePositions.first.x == _applePosition.x &&
        _snakePiecePositions.first.y == _applePosition.y) {
      return true;
    }

    return false;
  }

  bool _isBoardFilled() {
    const totalPiecesThatBoardCanFit =
        (boardSize * boardSize) / (pieceSize * pieceSize);
    if (_snakePiecePositions.length == totalPiecesThatBoardCanFit) {
      return true;
    }

    return false;
  }

  Point _getNewHeadPosition() {
    Point newHeadPos;

    switch (_direction) {
      case Direction.left:
        var currentHeadPos = _snakePiecePositions.first;
        newHeadPos = Point(currentHeadPos.x - 1, currentHeadPos.y);
        break;

      case Direction.right:
        var currentHeadPos = _snakePiecePositions.first;
        newHeadPos = Point(currentHeadPos.x + 1, currentHeadPos.y);
        break;

      case Direction.up:
        var currentHeadPos = _snakePiecePositions.first;
        newHeadPos = Point(currentHeadPos.x, currentHeadPos.y - 1);
        break;

      case Direction.down:
        var currentHeadPos = _snakePiecePositions.first;
        newHeadPos = Point(currentHeadPos.x, currentHeadPos.y + 1);
        break;
    }

    return newHeadPos;
  }

  void _handleTap(TapUpDetails tapUpDetails) {
    switch (_gameState) {
      case GameState.splash:
        _moveFromSplashToRunningState();
        break;
      case GameState.running:
        _changeDirectionBasedOnTap(tapUpDetails);
        break;
      case GameState.victory:
        _changeGameState(GameState.splash);
        break;
      case GameState.failure:
        _changeGameState(GameState.splash);
        break;
    }
  }

  void _moveFromSplashToRunningState() {
    _generateFirstSnakePosition();
    _generateNewApple();
    _eventsQueue = [];
    _direction = Direction.up;
    _directionChanged = false;
    _changeGameState(GameState.running);
    _timer = Timer.periodic(const Duration(milliseconds: 500), _onTimerTick);
    _eventsTimer =
        Timer.periodic(const Duration(milliseconds: 50), _onEventsTimerTick);
  }

  void _changeDirectionBasedOnTap(TapUpDetails tapUpDetails) {
    RenderBox? getBox = context.findRenderObject() as RenderBox?;
    if (getBox == null) return;
    var localPosition = getBox.globalToLocal(tapUpDetails.globalPosition);
    final x = (localPosition.dx / pieceSize).round();
    final y = (localPosition.dy / pieceSize).round();

    final currentHeadPos = _snakePiecePositions.first;

    switch (_direction) {
      case Direction.left:
        if (y < currentHeadPos.y) {
          setState(() {
            _direction = Direction.up;
          });
          return;
        }

        if (y > currentHeadPos.y) {
          setState(() {
            _direction = Direction.down;
          });
          return;
        }
        break;

      case Direction.right:
        if (y < currentHeadPos.y) {
          setState(() {
            _direction = Direction.up;
          });
          return;
        }

        if (y > currentHeadPos.y) {
          setState(() {
            _direction = Direction.down;
          });
          return;
        }
        break;

      case Direction.up:
        if (x < currentHeadPos.x) {
          setState(() {
            _direction = Direction.left;
          });
          return;
        }

        if (x > currentHeadPos.x) {
          setState(() {
            _direction = Direction.right;
          });
          return;
        }
        break;

      case Direction.down:
        if (x < currentHeadPos.x) {
          setState(() {
            _direction = Direction.left;
          });
          return;
        }

        if (x > currentHeadPos.x) {
          setState(() {
            _direction = Direction.right;
          });
          return;
        }
        break;
    }
  }

  void _changeGameState(GameState gameState) {
    setState(() {
      _gameState = gameState;
    });
  }

  void _generateFirstSnakePosition() {
    setState(() {
      const midPoint = (boardSize / pieceSize / 2);
      _snakePiecePositions = [
        Point(midPoint, midPoint - 2),
        Point(midPoint, midPoint - 1),
        Point(midPoint, midPoint),
        Point(midPoint, midPoint + 1),
        Point(midPoint, midPoint + 2),
      ];
    });
  }

  void _generateNewApple() {
    Random rng = Random();
    var min = 0;
    var max = boardSize ~/ pieceSize;
    var nextX = min + rng.nextInt(max - min);
    var nextY = min + rng.nextInt(max - min);

    var newApple = Point(nextX.toDouble(), nextY.toDouble());

    var validPosition = true;
    for (var snakePiece in _snakePiecePositions) {
      if (snakePiece.x == newApple.x && snakePiece.y == newApple.y) {
        validPosition = false;
      }
    }
    if (validPosition) {
      setState(() {
        _applePosition = newApple;
      });
    } else {
      _generateNewApple();
    }
  }

  KeyEventResult _handleKey(RawKeyEvent event) {
    _eventsQueue.add(event);
    return KeyEventResult.ignored;
  }

  KeyEventResult _changeDirectionBasedOnKey(RawKeyEvent event) {
    KeyEventResult result = KeyEventResult.ignored;
    switch (event.logicalKey.keyLabel) {
      case "A":
      case "Arrow Left":
        if (!_directionChanged && _direction != Direction.right) {
          setState(() {
            _direction = Direction.left;
            _directionChanged = true;
          });
          result = KeyEventResult.handled;
        }
        break;

      case "D":
      case "Arrow Right":
        if (!_directionChanged && _direction != Direction.left) {
          setState(() {
            _direction = Direction.right;
            _directionChanged = true;
          });
          result = KeyEventResult.handled;
        }
        break;

      case "W":
      case "Arrow Up":
        if (!_directionChanged && _direction != Direction.down) {
          setState(() {
            _direction = Direction.up;
            _directionChanged = true;
          });
          result = KeyEventResult.handled;
        }
        break;

      case "S":
      case "Arrow Down":
        if (!_directionChanged && _direction != Direction.up) {
          setState(() {
            _direction = Direction.down;
            _directionChanged = true;
          });
          result = KeyEventResult.handled;
        }
        break;
    }
    return result;
  }

  void _evaluateKey() {
    KeyEventResult result = KeyEventResult.ignored;
    while (result == KeyEventResult.ignored && _eventsQueue.isNotEmpty) {
      result = _changeDirectionBasedOnKey(_eventsQueue.first);
      _eventsQueue.removeAt(0);
    }
  }

  bool _isSnakeCollision() {
    var currentHeadPos = _snakePiecePositions.first;

    var snakeTale = [..._snakePiecePositions];
    snakeTale.remove(currentHeadPos);

    for (var pos in snakeTale) {
      if (currentHeadPos.x == pos.x && currentHeadPos.y == pos.y) {
        return true;
      }
    }

    return false;
  }
}
