import 'package:flutter/widgets.dart';
import 'package:snake/game_constants.dart';

class Failure extends StatelessWidget {
  const Failure({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var backgroundColor = themePalette['color3'] ?? const Color(0xFFFFFFFF);
    var textColor = themePalette['color5'] ?? const Color(0xFFFF0C0C);
    return Container(
      color: backgroundColor,
      width: boardSize,
      height: boardSize,
      padding: const EdgeInsets.all(textPadding),
      child: Center(
        child: Text("Game over! Tap to play again!",
            textAlign: TextAlign.center, style: TextStyle(color: textColor)),
      ),
    );
  }
}
