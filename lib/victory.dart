import 'package:flutter/widgets.dart';
import 'package:snake/game_constants.dart';

class Victory extends StatelessWidget {
  const Victory({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var backgroundColor = themePalette['color3'] ?? const Color(0xFFFFFFFF);
    var textColor = themePalette['color5'] ?? const Color(0xFF50C878);
    return Container(
      color: backgroundColor,
      width: boardSize,
      height: boardSize,
      padding: const EdgeInsets.all(textPadding),
      child: Center(
        child: Text(
          "Victory! Tap to play again!",
          textAlign: TextAlign.center,
          style: TextStyle(color: textColor),
        ),
      ),
    );
  }
}
